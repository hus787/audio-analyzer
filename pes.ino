/* #include <arm_math.h> */
#include <stdint.h>
#include <stdbool.h>

#define PART_TM4C123GH6PM
#include "inc/tm4c123gh6pm.h"

// ###################################


#define NUM_SAMPLES  2048 //2048 
#define SAMPLING_FREQ  22300 //600
#define LED_REFRESH_RATE 21
#define AUD_IN_PIN PE_3
#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"


float aud_samp[NUM_SAMPLES];
float sample;
int sensorValue = 0;        // value read from the pot
int sample_index = 0; //
float g_HzPerBin;

#define INVERT_FFT				0
#define BIT_ORDER_FFT			1
float ti_hamming_window_vector[NUM_SAMPLES];



void ProcessData(void){
  
}




void initTimer()
{  
  // sampling
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
  ROM_TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);   // 32 bits Timer
  TimerIntRegister(TIMER0_BASE, TIMER_A, Timer0Isr);    // Registering  isr       
  ROM_TimerEnable(TIMER0_BASE, TIMER_A); 
  ROM_IntEnable(INT_TIMER0A); 
  ROM_TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);  
}


void Timer0Isr(void)
{
  
  ROM_TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);  // Clear the timer interrupt
  aud_samp[sample_index++] = analogRead(AUD_IN_PIN);
  if (sample_index==NUM_SAMPLES){
    digitalWrite(RED_LED, digitalRead(RED_LED) ^ 1);              // toggle LED pin
    ROM_TimerDisable(TIMER0_BASE, TIMER_A); 

  }
}


void setup()
{
  Serial.begin(9600);
  pinMode(RED_LED,OUTPUT);
  pinMode(BLUE_LED,OUTPUT); 
  initTimer();

}

void loop()
{  
  unsigned long ulPeriod;

  unsigned int Samp_Hz = SAMPLING_FREQ; 


  ROM_TimerLoadSet(TIMER0_BASE, TIMER_A,((SysCtlClockGet() / Samp_Hz)/ 2) -1);
  while(1){
    unsigned int i = 0;
    for (i=0; i<NUM_SAMPLES; i++){
      Serial.print(aud_samp[i]);
      Serial.print('\t');
      Serial.println(i);
    }
  }
}
